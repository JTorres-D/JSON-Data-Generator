package main

import (
	"math/rand"
	"encoding/json"
	"io/ioutil"
	"log"
	"time"
	"os"
)

type Record struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Age int8 `json:"age"`
	Color string `json:"color"`
	Data int `json:"data"`
}

func main() {
	///////////////////////////////////////////////////////////////////
	// How many records do you want generate?
	var recordsToGenerate = 350000
	// What will be the max age in record age attribute?
	var maxAge = 100
	// Which names do you want to include?
	var names = []string {
		"Jose", "Diego", "Arturo", "Eduardo", "Antonio",
		"Ana", "Abril", "Vanessa", "Maria", "Camila",
		"Alicia", "Francisco", "Daniel", "Arturo", "Josefina"}
	// Which last names do you want to include?
	var lastNames = []string {
		"Torres", "Perez", "Lopez", "Martínez", "Corona",
		"Méndez", "Bravo", "Peña", "Garcia", "Andrade",
		"Sandoval", "Avila", "Vargas", "Rivera", "Ramos",
		"Flores", "Gonzáles", "Hernandez", "Rodriguez", "Morales"}
	// What will be the file name to export?
	var fileName = "records"
	// Do you want a indented json file? (This affect the saving performance)
	var indented = true
	///////////////////////////////////////////////////////////////////

	log.Println("Generando archivo ...")
	log.Println(createRandomJson(recordsToGenerate, maxAge, names, lastNames, fileName, indented))
	log.Println("Exito! ")

}

func createRandomJson(
	recordsToGenerate int, maxAge int, names []string,
	lastNames []string, fileName string, indented bool) time.Duration {

	var startTime = time.Now()

	// Create an instance of Record that will be contain all records generated
	var jsonRecords []Record

	// Initialize globally random seed to get fresh random values in all rand functions
	rand.Seed(time.Now().UnixNano())

	// Generates records and append them in a jsonRecords
	for i := 0; i < recordsToGenerate; i++ {
		jsonRecords = append(jsonRecords, Record{
			ID: i,
			Name: randomName(names, lastNames),
			Age: randomAge(maxAge),
			Color: randomColor(),
			Data: randomData(),
		})
	}

	// Printing out json neatly to demonstrate
	if indented {
		jsonBytes, err := json.MarshalIndent(jsonRecords, "", "\t")

		if err != nil {
			log.Fatal(err)
		} else {
			// Writing final json to file in records.json saving it in current directory
			err = ioutil.WriteFile(fileName + ".json", jsonBytes, os.FileMode(0644))
			if err != nil {
				log.Fatal(err)
			}
		}
	} else {
		jsonBytes, err := json.Marshal(jsonRecords)

		if err != nil {
			log.Fatal(err)
		} else {
			// Writing final json to file in records.json saving it in current directory
			err = ioutil.WriteFile(fileName + ".json", jsonBytes, os.FileMode(0644))
			if err != nil {
				log.Fatal(err)
			}
		}
	}


	var endTime = time.Now()

	return endTime.Sub(startTime)
}

/* Generates a random name based on given slices */
func randomName(names []string, lastNames []string) string {

	var name string	// Random name that will be created

	var randomName = rand.Intn(len(names)) // Random number between 0 and the names total length
	var randomLastName = rand.Intn(len(lastNames)) // Random number between 0 and the lastNames total length

	// Creates the name based in previous random variables
	name = names[randomName] + " " + lastNames[randomLastName]

	// Return generated name
	return name
}

/* Generates a random age between 0 and 99 */
func randomAge(maxAge int) int8 {
	// Generates random age between 0 and 99
	return int8( rand.Intn(maxAge) ) // Random Number
}

/* Generates a random color */
func randomColor() string {
	var randomColor = "Undefined"
	var randNumber = rand.Intn(10) // Generates a random number between 0 and 10

	var colors = []string{
						"Red", "Blue", "Green", "Orange", "Brown",
						"Grey", "Black", "White", "Purple", "Pink"}

	// Assign color based in random number
	randomColor = colors[randNumber]

	// Return the color assigned
	return randomColor
}

/* Generates a random data */
func randomData() int {
	return rand.Int() / 10e13
}